<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Order;
use App\Entity\Users;
use App\Entity\Ticket;

class OrderTest extends TestCase
{
    public function testIsTrue()
    {
        $order = new Order();
        $user = new Users();

        $order->setReference('REF123')
              ->setUserId($user)
              ->setCreatedAt(new \DateTimeImmutable('2024-05-12'));

        $this->assertTrue($order->getReference() === 'REF123');
        $this->assertTrue($order->getUserId() === $user);
        $this->assertTrue($order->getCreatedAt()->format('Y-m-d') === '2024-05-12');
    }

    public function testIsFalse()
    {
        $order = new Order();
        $user = new Users();

        $order->setReference('REF123')
              ->setUserId($user)
              ->setCreatedAt(new \DateTimeImmutable('2024-05-12'));

        $this->assertFalse($order->getReference() === 'INVALID_REF');
        $this->assertFalse($order->getUserId() === null);
        $this->assertFalse($order->getCreatedAt()->format('Y-m-d') === '2024-05-13');
    }

    public function testIsEmpty()
    {
        $order = new Order();

        $this->assertEmpty($order->getReference());
        $this->assertEmpty($order->getUserId());
        $this->assertEmpty($order->getCreatedAt());
    }
}
