<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Ticket;
use App\Entity\Product;
use App\Entity\Order;

class TicketTest extends TestCase
{
    public function testIsTrue()
    {
        $ticket = new Ticket();
        $product = new Product();
        $order = new Order();

        $ticket->setType('VIP')
               ->setProduct($product)
               ->setPrice(100.00)
               ->setOrdersId($order)
               ->setKeyTicket('ABC123');

        $this->assertTrue($ticket->getType() === 'VIP');
        $this->assertTrue($ticket->getProduct() === $product);
        $this->assertTrue($ticket->getPrice() === 100.00);
        $this->assertTrue($ticket->getOrdersId() === $order);
        $this->assertTrue($ticket->getKeyTicket() === 'ABC123');
    }

    public function testIsFalse()
    {
        $ticket = new Ticket();
        $product = new Product();
        $order = new Order();

        $ticket->setType('VIP')
               ->setProduct($product)
               ->setPrice(100.00)
               ->setOrdersId($order)
               ->setKeyTicket('ABC123');

        $this->assertFalse($ticket->getType() === 'General');
        $this->assertFalse($ticket->getProduct() === null);
        $this->assertFalse($ticket->getPrice() === 50.00);
        $this->assertFalse($ticket->getOrdersId() === null);
        $this->assertFalse($ticket->getKeyTicket() === 'DEF456');
    }

    public function testIsEmpty()
    {
        $ticket = new Ticket();

        $this->assertEmpty($ticket->getType());
        $this->assertEmpty($ticket->getProduct());
        $this->assertEmpty($ticket->getPrice());
        $this->assertEmpty($ticket->getOrdersId());
        $this->assertEmpty($ticket->getKeyTicket());
    }
}
