<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Product;
use App\Entity\Sport;
use App\Entity\Ticket;

class ProductTest extends TestCase
{
    public function testIsTrue()
    {
        $product = new Product();
        $sport = new Sport();

        $product->setDescription('Event ticket')
               ->setDate(new \DateTimeImmutable('2024-07-01'))
               ->setLocalisation('Paris')
               ->setNumberPlace(500)
               ->setPrice(50.00)
               ->setSport($sport);

        $this->assertTrue($product->getDescription() === 'Event ticket');
        $this->assertTrue($product->getDate()->format('Y-m-d') === '2024-07-01');
        $this->assertTrue($product->getLocalisation() === 'Paris');
        $this->assertTrue($product->getPrice() === 50.00);
        $this->assertTrue($product->getSport() === $sport);
    }

    public function testIsFalse()
    {
        $product = new Product();
        $sport = new Sport();

        $product->setDescription('Event ticket')
               ->setDate(new \DateTimeImmutable('2024-07-01'))
               ->setLocalisation('Paris')
               ->setNumberPlace(1000)
               ->setPrice(50.00)
               ->setSport($sport);

        $this->assertFalse($product->getDescription() === 'Concert ticket');
        $this->assertFalse($product->getDate()->format('Y-m-d') === '2024-07-02');
        $this->assertFalse($product->getLocalisation() === 'London');
        $this->assertFalse($product->getNumberPlace() === 2000);
        $this->assertFalse($product->getPrice() === 100.00);
        $this->assertFalse($product->getSport() === null);
    }

    public function testIsEmpty()
    {
        $product = new Product();

        $this->assertEmpty($product->getDescription());
        $this->assertEmpty($product->getDate());
        $this->assertEmpty($product->getLocalisation());
        $this->assertEmpty($product->getNumberPlace());
        $this->assertEmpty($product->getPrice());
        $this->assertEmpty($product->getSport());
    }
}
