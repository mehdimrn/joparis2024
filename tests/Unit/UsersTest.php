<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Users;

class UsersTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new Users();

        $user->setEmail("true@gmail.com")
             ->setRoles(["ROLE_USER"])
             ->setPassword("password")
             ->setFirstname("firstname")
             ->setLastname("lastname")
             ->setKeyAccount("keyaccount");

        $this->assertTrue($user->getEmail() === 'true@gmail.com');
        $this->assertTrue($user->getRoles() === ["ROLE_USER"]);
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getKeyAccount() === 'keyaccount');

    }

    public function testIsFalse()
    {
        $user = new Users();

        $user->setEmail("true@gmail.com")
             ->setRoles(["ROLE_USER"])
             ->setPassword("password")
             ->setFirstname("firstname")
             ->setLastname("lastname")
             ->setKeyAccount("keyaccount");

        $this->assertFalse($user->getEmail() === 'false@gmail.com');
        $this->assertFalse($user->getRoles() === ["false"]);
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getKeyAccount() === 'false');

    }

    public function testIsEmpty()
    {
        
        $user = new Users();

        $user->setEmail('')
             ->setRoles([""])
             ->setPassword("")
             ->setFirstname("")
             ->setLastname("")
             ->setKeyAccount("");

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getKeyAccount());

    }
}
