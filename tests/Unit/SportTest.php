<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Sport;
use App\Entity\Product;

class SportTest extends TestCase
{
    public function testIsTrue()
    {
        $sport = new Sport();
        $sport->setName('Football')
              ->setPicture('football.jpg');

        $this->assertTrue($sport->getName() === 'Football');
        $this->assertTrue($sport->getPicture() === 'football.jpg');
    }

    public function testIsFalse()
    {
        $sport = new Sport();
        $sport->setName('Football')
              ->setPicture('football.jpg');

        $this->assertFalse($sport->getName() === 'Basketball');
        $this->assertFalse($sport->getPicture() === 'basketball.jpg');
    }

    public function testIsEmpty()
    {
        $sport = new Sport();
        $sport->setName('')
              ->setPicture('');

        $this->assertEmpty($sport->getName());
        $this->assertEmpty($sport->getPicture());
    }

    public function testProductsCollection(): void
    {
        $sport = new Sport();
        $product1 = new Product();
        $product2 = new Product();

        $this->assertCount(0, $sport->getProducts());

        $sport->addProduct($product1);

        $this->assertCount(1, $sport->getProducts());
        $this->assertTrue($sport->getProducts()->contains($product1));

        $sport->addProduct($product2);

        $this->assertCount(2, $sport->getProducts());
        $this->assertTrue($sport->getProducts()->contains($product2));

        $sport->removeProduct($product1);

        $this->assertCount(1, $sport->getProducts());
        $this->assertFalse($sport->getProducts()->contains($product1));
    }
}
