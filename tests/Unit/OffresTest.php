<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Offres;

class OffresTest extends TestCase
{
    public function testIsTrue()
    {
        $offre = new Offres();

        $offre->setName('Offre A')
              ->setMultiplicateur(1.5);

        $this->assertTrue($offre->getName() === 'Offre A');
        $this->assertTrue($offre->getMultiplicateur() === 1.5);
    }

    public function testIsFalse()
    {
        $offre = new Offres();

        $offre->setName('Offre A')
              ->setMultiplicateur(1.5);

        $this->assertFalse($offre->getName() === 'Offre B');
        $this->assertFalse($offre->getMultiplicateur() === 2.0);
    }

    public function testIsEmpty()
    {
        $offre = new Offres();

        $this->assertEmpty($offre->getName());
        $this->assertEmpty($offre->getMultiplicateur());
    }
}
