<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use App\Entity\Order;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use DateTimeImmutable;
use App\Entity\Ticket;

class PaymentController extends AbstractController
{
    // Propriété pour générer les URLs
    private UrlGeneratorInterface $generator;

    // Injection du service UrlGeneratorInterface dans le constructeur
    public function __construct(UrlGeneratorInterface $generator)
    {
        $this->generator = $generator;
    }

    // Méthode pour effectuer le paiement avec Stripe
    #[Route('/order/create-session-stripe/{reference}', name: 'payment_stripe')]
    public function stripeCheckout($reference, SessionInterface $session, ProductRepository $productRepository): RedirectResponse
    {
        // Supprime le panier de la session
        $session->remove('panier');
        $panier = $session->get('order', []);

        // Crée une nouvelle commande
        $order = new Order();
        $order->setUserId($this->getUser());
        $order->setReference($reference);
        $order->setCreatedAt(new DateTimeImmutable('now'));

        // Parcours le panier pour créer les tickets associés à la commande
        foreach ($panier as $item) {
            for ($i = $item['quantity']; $i > 0; $i--) {
                $product = $productRepository->find($item['product_id']);
                $price = $item['price'];
                $type = $item['type'];

                // Crée un ticket associé au produit et l'ajoute à la commande
                $ticket = (new Ticket())
                    ->setProduct($product)
                    ->setType($type)
                    ->setPrice($price)
                    ->setKeyTicket(uniqid('2_', true));

                $order->addTicket($ticket);
            }
        }

        // Initialise un tableau pour les produits Stripe
        $productStripe = [];

        // Construit les produits Stripe à partir des tickets de la commande
        $cpt = 0;
        foreach ($order->getTickets()->getValues() as $product) {
            $cpt += 1;
            $price = (int) ($product->getPrice() * 100);
            $productStripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $price,
                    'product_data' => [
                        'name' => $product->getProduct()->getDescription() . " Ticket #" . $cpt
                    ]
                ],
                'quantity' => 1
            ];
        }

        // Clé secrète Stripe
        $stripeSecretKey = 'sk_test_51P6E6bIRIXmpakQrVNs6xSG274UhmSSLGxFcukkrw626Ed7KROfhQBYkKlNiGsS3eexFflu91O0MbdGEQfyXA7DW0001y6rhDb';

        // Configure l'API Stripe
        Stripe::setApiKey($stripeSecretKey);

        // Crée une session de paiement avec Stripe
        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => [[
                $productStripe
            ]],
            'mode' => 'payment',
            'success_url' => $this->generator->generate('payment_success', ['reference' => $order->getReference()], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' =>  $this->generator->generate('payment_error', ['reference' => $order->getReference()], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        // Redirige l'utilisateur vers l'URL de la session de paiement Stripe
        return new RedirectResponse($checkout_session->url);
    }

    // Méthode exécutée en cas de succès du paiement
    #[Route('/order/success/{reference}', name: 'payment_success')]
    public function stripeSuccess($reference, SessionInterface $session, ProductRepository $productRepository, EntityManagerInterface $em): Response
    {

        $panier = $session->get('order', []);
        // Crée une nouvelle commande
        $order = new Order();
        $order->setUserId($this->getUser());
        $order->setReference($reference);
        $order->setCreatedAt(new DateTimeImmutable('now'));

        // Parcours le panier pour créer les tickets associés à la commande
        foreach ($panier as $item) {
            for ($i = $item['quantity']; $i > 0; $i--) {
                $product = $productRepository->find($item['product_id']);
                $price = $item['price'];
                $type = $item['type'];

                // Crée un ticket associé au produit et l'ajoute à la commande
                $ticket = (new Ticket())
                    ->setProduct($product)
                    ->setType($type)
                    ->setPrice($price)
                    ->setKeyTicket(uniqid('2_', true));

                $order->addTicket($ticket);
            }
        }


        // Persiste et flush la commande
        $em->persist($order);
        $em->flush();
        $session->remove('order');

        // Affiche la page de succès du paiement
        return $this->render('order/success.html.twig');
    }

    // Méthode exécutée en cas d'erreur lors du paiement
    #[Route('/order/error/{reference}', name: 'payment_error')]
    public function stripeError($reference, SessionInterface $session): Response
    {



        $session->remove('order');
        // Affiche la page d'erreur du paiement
        return $this->render('order/error.html.twig');
    }
}
