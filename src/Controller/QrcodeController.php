<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\TicketRepository;
use Symfony\Component\Routing\Attribute\Route;


class QrcodeController extends AbstractController
{
    #[Route('/qrcode/{key}', name: 'app_qrcode')]
    public function decode($key, TicketRepository $ticketRepository): Response
    {
        // Décodage de la clé de chiffrement reçue depuis l'URL
        $encryptedData = base64_decode($key);
        $method = "AES-128-CTR";
        $keycrypt = "encryptionKey123";
        $options = 0;
        $iv = '1234567891011121';
        $decryptedData = openssl_decrypt($encryptedData, $method, $keycrypt, $options, $iv);
        $decryptedKeys = str_split($decryptedData, 25);


        $reponse = "TICKET NON OFFICIEL";

        if (strlen($key) == 92) {

            // Recherche du ticket correspondant aux clés décryptées
            $ticket = $ticketRepository->findOneBy(array('keyTicket' => $decryptedKeys[0]));

            // Vérification de l'authenticité du ticket
            if ($ticket) {
                if ($ticket->getOrdersId()->getUserId()->getKeyAccount() == $decryptedKeys[1]) {

                    $nom = $ticket->getOrdersId()->getUserId()->getLastname();
                    $prenom = $ticket->getOrdersId()->getUserId()->getFirstname();
                    $mail = $ticket->getOrdersId()->getUserId()->getEmail();
                    $type = $ticket->getType();
                    $reponse = "TICKET OFFICIEL";

                    // Rendu de la vue Twig pour afficher le résultat
                    return $this->render('qrcode/index.html.twig', [
                        'key' => $decryptedData,
                        'reponse' => $reponse,
                        'nom' => $nom,
                        'prenom' => $prenom,
                        'mail' => $mail,
                        'type' => $type,
                    ]);
                }
            }
        }

        // Rendu de la vue Twig pour afficher le résultat
        return $this->render('qrcode/index.html.twig', [
            'reponse' => $reponse,
        ]);
    }
}
