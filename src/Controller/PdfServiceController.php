<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\TicketRepository;
use Symfony\Component\Routing\Attribute\Route;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Color\Color;

class PdfServiceController extends AbstractController
{
    #[Route('/pdf/service/{id}', name: 'pdf_service')]
    public function index($id, TicketRepository $ticketRepository): Response
    {
        // Récupération du ticket en fonction de son ID
        $ticket = $ticketRepository->findOneBy(array('id' => $id));
        // Récupération de la référence de la commande associée au ticket
        $ref = $ticket->getOrdersId()->getReference();

        // Récupération des clés de chiffrement du ticket et du compte utilisateur
        $keyticket = $ticket->getKeyTicket();
        $keyaccount = $ticket->getOrdersId()->getUserId()->getKeyAccount();

        // Concaténation des clés pour former la clé de chiffrement
        $skcode = $keyticket . $keyaccount;

        // Chiffrement de la clé avec openssl_encrypt et base64_encode
        $method = "AES-128-CTR";
        $key = "encryptionKey123";
        $options = 0;
        $iv = '1234567891011121';
        $encryptedData = openssl_encrypt($skcode, $method, $key, $options, $iv);
        $encryptedDataBase64 = base64_encode($encryptedData);

        // Génération du QR code avec Endroid/QrCode
        $encoding = new Encoding('UTF-8');
        // Si l'application est lancé en local remplace la ligne suivante par:
        // $qrCode = new QrCode('127.0.0.1:8000/qrcode/' . $encryptedDataBase64);
        $qrCode = new QrCode('https://jeuxolympique2024-8151e87c182f.herokuapp.com/qrcode/' . $encryptedDataBase64);
        $qrCode->setSize(200);
        $qrCode->setMargin(10);
        $qrCode->setEncoding($encoding);
        $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::High);
        $qrCode->setForegroundColor(new Color(0, 0, 0));
        $qrCode->setBackgroundColor(new Color(255, 255, 255));
        $writer = new PngWriter();
        $result = $writer->write($qrCode);
        $dataUri = $result->getDataUri();

        // Création d'un tableau contenant les informations du ticket
        $myTicket[] = [
            'reference' => $ticket->getOrdersId()->getReference(),
            'idTicket' => $ticket->getId(),
            'sport' => $ticket->getProduct()->getSport()->getName(),
            'description' => $ticket->getProduct()->getDescription(),
            'type' => $ticket->getType(),
            'price' => $ticket->getPrice(),
            'date' => $ticket->getProduct()->getDate(),
            'localisation' => $ticket->getProduct()->getLocalisation(),
        ];

        // Rendu de la vue Twig pour générer le PDF
        $html = $this->renderView('pdf_service/index.html.twig', [
            'ticket' => $myTicket,
            'qrcode' => $dataUri,
        ]);

        // Utilisation de Dompdf pour générer le PDF
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // Envoi du PDF en réponse avec stream
        $dompdf->stream('ticket' . $ref, ["attachement" => false], 200, ['Content-Type' => 'application/pdf']);
        return new Response('', 200, [
            'Content-Type' => 'application/pdf',
        ]);

        // Le code ci-dessous ne sera jamais exécuté car le PDF est envoyé avant
        // Donc vous pouvez le supprimer pour éviter une exécution inutile
        return $this->render('pdf_service/index.html.twig', [
            'ticket' => $myTicket,
            'qrcode' => $dataUri,
        ]);
    }
}
