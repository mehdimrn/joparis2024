<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

#[Route('/commandes', name: 'app_order_')]

class OrderController extends AbstractController
{
    #[Route('/ajout', name: 'add')]
    public function add(SessionInterface $session, ProductRepository $productRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $panier = $session->get('panier', []);

        $session->set('order', $panier);



        // Si le panier est vide (le bouton valider panier ne devrait pas apparaitre)
        if ($panier === []) {
            $this->addFlash('message', 'Votre panier est vide');
            return $this->redirectToRoute('app_accueil');
        }

        //recapitulatif de commande pour la vue
        $data = [];
        $total = 0;
        foreach ($panier as $item) {
            $product = $productRepository->find($item['product_id']);
            if ($product) {
                $data[] = [
                    'product' => $product,
                    'type' => $item['type'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],

                ];
                $total += ($item['price'] * $item['quantity']);
            }
        }

      
        $reference = uniqid('3_', true);
        

        return $this->render('order/index.html.twig', [
            'data' => $data,
            'total' => $total,
            'reference' => $reference,

        ]);
    }
}
