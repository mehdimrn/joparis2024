<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\Sport;
use App\Repository\ProductRepository;
use App\Repository\OffresRepository;
use App\Repository\SportRepository;

class ProductController extends AbstractController
{
    #[Route('/product', name: 'app_product')]
    public function index(): Response
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }

    #[Route('/products/{sport}', name: 'app_products_by_sport')]
    public function productsBySport($sport, SportRepository $sportRepository): Response
    {
        
        $sportexiste = $sportRepository->find($sport);

        if ($sportexiste == null) {
            $this->addFlash('message', "Ce Produit n'existe pas");
            return $this->redirectToRoute('app_sport');
        }

        return $this->render('product/products_by_sport.html.twig', [
            'sport' => $sportexiste,
        ]);
    }


    #[Route('/acheter-ticket/{id}', name: 'acheter_ticket')]
    public function acheterTicket($id, ProductRepository $productRepository, OffresRepository $offresRepository): Response
    {

        // Récupérer le produit à partir de son identifiant
        $product = $productRepository->find($id);

        // Récupérer les offres
        $offres = $offresRepository->findAll();

        // Vérifier si le produit existe
        if (!$product) {
            $this->addFlash('message', "Ce Produit n'existe pas");
            return $this->redirectToRoute('app_sport');
        }


        // Rendre la vue appropriée en utilisant le produit récupéré
        return $this->render('product/acheter_ticket.html.twig', [
            'product' => $product,
            'offres' => $offres,
        ]);
    }
}
