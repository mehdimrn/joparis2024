<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\SportRepository;

class SportController extends AbstractController
{
    #[Route('/sport', name: 'app_sport')]
    public function index(SportRepository $sportRepository): Response
    {

        $sports = $sportRepository->findAll();




        return $this->render('sport/index.html.twig', [
            'controller_name' => 'SportController',
            'sports' => $sports,
        ]);
    }

}
