<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\TicketRepository;

class DashboardUserController extends AbstractController
{
    #[Route('/dashboard/user', name: 'app_dashboard_user')]
    public function index(TicketRepository $ticketRepository, OrderRepository $orderRepository): Response
    {
        // Vérifie si l'utilisateur a le rôle ROLE_USER ou ROLE_ADMIN
        $this->denyAccessUnlessGranted('ROLE_USER', 'ROLE_ADMIN');

        // Récupère l'ID de l'utilisateur actuel
        $idUser = $this->getUser()->getId();

        // Récupère les commandes de l'utilisateur à partir de son ID
        $myOrders = $orderRepository->findBy(array('userId' => $idUser));

        // Tableaux pour stocker les tickets de l'utilisateur et ses commandes
        $myTickets = [];
        $orders = [];

        // Boucle à travers chaque commande de l'utilisateur
        foreach ($myOrders as $item) {
            $id = $item->getId();

            // Ajoute les détails de la commande à un tableau
            $orders[] = [
                'id' => $item->getId(),
                'reference' => $item->getReference(),
                'createdAt' => $item->getCreatedAt(),
            ];

            // Récupère les tickets associés à cette commande
            $ticket = $ticketRepository->findBy(array('orders_id' => $id));

            // Boucle à travers chaque ticket et ajoute ses détails au tableau
            foreach ($ticket as $item) {
                $myTickets[] = [
                    'reference' => $item->getOrdersId()->getReference(),
                    'idTicket' => $item->getId(),
                    'sport' => $item->getProduct()->getSport()->getName(),
                    'description' => $item->getProduct()->getDescription(),
                    'type' => $item->getType(),
                    'price' => $item->getPrice(),
                    'date' => $item->getProduct()->getDate(),
                    'localisation' => $item->getProduct()->getLocalisation(),
                ];
            }
        }

        // Rendu de la vue Twig avec les données récupérées
        return $this->render('dashboard_user/index.html.twig', [
            'myOrders' => $orders,
            'myTickets' => $myTickets,
        ]);
    }
}
