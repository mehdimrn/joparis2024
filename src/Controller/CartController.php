<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cart', name: 'cart_')]
class CartController extends AbstractController
{
    // Affiche le contenu du panier
    #[Route('/', name: 'index')]
    public function index(SessionInterface $session, ProductRepository $productRepository, Request $request)
    {
        // Récupère le panier de la session
        $panier = $session->get('panier', []);

        // Initialise les variables
        $data = [];
        $total = 0;

        // Transforme les ID des produits du panier en entités Product
        foreach ($panier as $item) {
            $product = $productRepository->find($item['product_id']);
            if ($product) {
                $data[] = [
                    'product' => $product,
                    'sport' =>$product->getSport()->getName(),
                    'type' => $item['type'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                ];
                $total += ($item['price'] * $item['quantity']);
            }
        }

        // Affiche la vue avec les données du panier
        return $this->render('cart/index.html.twig', [
            'data' => $data,
            'total' => $total,
        ]);
    }

    // Ajoute un ticket au panier
    #[Route('/add/{id}', name: 'add')]
    public function add(Product $product, SessionInterface $session, Request $request)
    {
        // Récupère le type de ticket depuis la requête
        $type = $request->query->get('type');

        // Récupère le prix de ticket depuis la requête
        $price = $request->query->get('price');

        // Récupère le panier de la session
        $panier = $session->get('panier', []);
 

        // Ajoute le ticket au panier
        $found = false;
        foreach ($panier as &$item) {
            if ($item["product_id"] === $product->getId() && $item["type"] === $type) {
                $item['quantity']++;
                $found = true;
                break;
            }
        }

        if (!$found) {
            $panier[] = [
                'product_id' => $product->getId(),
                'type' => $type,
                'quantity' => 1,
                'price' => $price,
            ];
        }

        // Met à jour le panier dans la session
        $session->set('panier', $panier);

        // Redirige vers la page du panier
        return $this->redirectToRoute('cart_index');
    }

    // Supprime un ticket du panier
    #[Route('/remove/{id}', name: 'remove')]
    public function remove(Product $product, SessionInterface $session, Request $request)
    {
        // Récupère le type de ticket depuis la requête
        $type = $request->query->get('type');

        // Récupère le panier de la session
        $panier = $session->get('panier', []);

        // Parcourt le panier pour supprimer le ticket spécifié
        foreach ($panier as $key => &$item) {
            if ($item["product_id"] === $product->getId() && $item["type"] === $type) {
                if ($item["quantity"] > 1) {
                    $item['quantity']--;
                } else {
                    unset($panier[$key]);
                }
                break;
            }
        }

        // Met à jour le panier dans la session
        $session->set('panier', $panier);

        // Redirige vers la page du panier
        return $this->redirectToRoute('cart_index');
    }

    // Supprime complètement un type de ticket du panier
    #[Route('/delete{id}', name: 'delete')]
    public function delete(Product $product, SessionInterface $session, Request $request)
    {
        // Récupère le type de ticket depuis la requête
        $type = $request->query->get('type');

        // Récupère le panier de la session
        $panier = $session->get('panier', []);

        // Parcourt le panier pour supprimer tous les tickets du type spécifié
        foreach ($panier as $key => &$item) {
            if ($item["product_id"] === $product->getId() && $item["type"] === $type) {
                unset($panier[$key]);
                break;
            }
        }

        // Met à jour le panier dans la session
        $session->set('panier', $panier);

        // Redirige vers la page du panier
        return $this->redirectToRoute('cart_index');
    }

    // Vide complètement le panier
    #[Route('/empty', name: 'empty')]
    public function empty(SessionInterface $session)
    {
        // Supprime le panier de la session
        $session->remove('panier');

        // Redirige vers la page du panier
        return $this->redirectToRoute('cart_index');
    }
}
