<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TicketRepository::class)]
class Ticket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $type = null;

    #[ORM\ManyToOne(inversedBy: 'tickets')]
    private ?Product $Product = null;

    #[ORM\Column(nullable: true)]
    private ?float $Price = null;

    #[ORM\ManyToOne(inversedBy: 'tickets')]
    private ?Order $orders_id = null;

    #[ORM\Column(length: 100)]
    private ?string $keyTicket = null;

    public function getId(): ?int
    {
        return $this->id;
    }

   
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->Product;
    }

    public function setProduct(?Product $Product): static
    {
        $this->Product = $Product;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->Price;
    }

    public function setPrice(?float $Price): static
    {
        $this->Price = $Price;

        return $this;
    }

    public function getOrdersId(): ?Order
    {
        return $this->orders_id;
    }

    public function setOrdersId(?Order $orders_id): static
    {
        $this->orders_id = $orders_id;

        return $this;
    }

    public function getKeyTicket(): ?string
    {
        return $this->keyTicket;
    }

    public function setKeyTicket(string $keyTicket): static
    {
        $this->keyTicket = $keyTicket;

        return $this;
    }
}
